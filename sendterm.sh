#!/bin/bash
set -e

function mainpid() {
  cat /run/haproxy.pid || true
}

cycles=0

while sleep "0.01"; do

  OLDPID=$(mainpid)

  if (( OLDPID != 0 )); then
    echo "Stopping haproxy $OLDPID"
    kill "$OLDPID" || true
  fi

  k=0
  while true; do
    if (( ++k > 200 )); then
      echo "-------------------------"
      echo "race hit: did not terminate quick enough"
      echo "cycle counter: $cycles"
      exit 1
    fi
    sleep "0.01"
  PID=$(mainpid)
    if (( PID != OLDPID || PID == 0 )); then
      break
    else
      echo "waiting"
    fi
  done

  echo cycle counter: $(( ++cycles ))
done
